# Dmenu Scripts
My dmenu scripts repository where I put some my dmenu scripts.

## Installation
If you run Arch Linux or Arch Linux based distro and you have enabled my [op-arch-repo](https://gitlab.com/orasponka/op-arch-repo) you can download these scripts with pacman.
```
$ sudo pacman -S <script name>
```
Or if you are not, run commands below
```
$ git clone https://gitlab.com/orasponka/dmenu-scripts
$ cd dmenu-scripts
$ chmod +x dm-fontviewer dm-youtube-downloader
$ ./dm-fontviewer 
$ ./dm-youtube-downloader
```
